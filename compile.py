import sys
from pathlib import Path
import argparse
import subprocess

# [instructions spec]
# specific instructions' compiler, as a function return a tuple of:
# (code_section_str, variable_name_list)
def ADD(param_list):
    a, b, res = param_list
    return ('{} = {} + {};\n'.format(res, a, b), param_list)

def SUB(param_list):
    a, b, res = param_list
    return ('{} = {} - {};\n'.format(res, a, b), param_list)
    
def MUL(param_list):
    a, b, res = param_list
    return ('{} = {} * {};\n'.format(res, a, b), param_list)

def DIV(param_list):
    a, b, res = param_list
    return ('{} = {} / {};\n'.format(res, a, b), param_list)

def prompt(varname):
    return 'printf("Enter {} = ");  scanf("%d", &{});\n'.format(varname, varname)

def INPUT(param_list):
    prompt_stmts = list(map(prompt, param_list))
    stmt_section = ''.join(prompt_stmts)
    return (stmt_section, param_list)

def report(varname):
    return 'printf("Result {} = %d\\n", {});\n'.format(varname, varname)
#                                  ^ escape the backslash here

def OUTPUT(param_list):
    report_stmts = list(map(report, param_list))
    stmt_section = ''.join(report_stmts)
    return (stmt_section, param_list)

instruction_dict = dict(
    ADD = ADD,
    SUB = SUB,
    MUL = MUL,
    DIV = DIV,
    INPUT = INPUT,
    OUTPUT = OUTPUT
)
# [/instructions spec]

def lexing(instructionStr):
    # return syntax representation structure for given instruction string
    # for simplicity, here is not proper syntax tree, just a list of tokens
    return instructionStr.rstrip('\n').split(' ')

def compile_c(c_filename):
    result = subprocess.run(['gcc', c_filename])
    return result.returncode

def main():

    # parse command line arguments

    parser = argparse.ArgumentParser(description='Stupid compiler. Refer to README.md for further details.')
    parser.add_argument('-o', '--output', help="Output filename of target C file (default to source file's basename with .c suffix)")
    parser.add_argument('-c', '--compile-c', action='store_true', help='Directly compile target C file to executable (Linux only, require GCC, generate a.out)')
    parser.add_argument('-r', '--run', action='store_true', help='Directly compile to executable then execute it (Linux only)')
    parser.add_argument('-b', '--beautify', action='store_true', help='Format target C code following the prominent style guide from "The C Programming Language" (Linux only, require GNU indent)')
    parser.add_argument('filename', help="Specify source filename")

    args = parser.parse_args()

    # open file given the name from commandline argument

    source_filename = args.filename
    source_file = open(source_filename)

    # list of "body" statements that actually do computation and I/O
    working_stmts = []

    # set of variable name to declare
    varnames = set()

    for line in source_file:
        # for each line in source, compile it into corresponding C statement section
        token_list = lexing(line)
        instruction_type = token_list[0] # first token
        param_list = token_list[1:]      # rest tokens
        instruction = instruction_dict[instruction_type]
        (stmt_section, new_varnames) = instruction(param_list)
        working_stmts.append(stmt_section)
        for varname in new_varnames:
            varnames.add(varname)
            
    # build one declare statement from accumulated set of variable name
    declare_stmt = 'int ' + ', '.join(sorted(varnames)) + ';\n'
    
    leading_boilerplate = '''
#include <stdio.h>

int main() {
'''

    trailing_boilerplate = '''
return 0;
}
'''

    # write target C file with the same basename as source file
    if args.output:
        target_filename = args.output
    else:
        target_filename = Path(source_filename).stem + '.c'

    target_file = open(target_filename, 'w')

    target_file.write(leading_boilerplate)
    target_file.write(declare_stmt)
    for stmt in working_stmts:
        target_file.write(stmt)
    target_file.write(trailing_boilerplate)

    target_file.close()

    # Format target C code
    if args.beautify:
        subprocess.run(['indent', '-kr', target_filename])

    # Directly compile it
    if args.compile_c:
        compile_c(target_filename)
    
    # Or run it altogether
    if args.run:
        returncode = compile_c(target_filename)
        if returncode == 0:
            subprocess.run(['./a.out'])

if __name__ == '__main__':
    main()