# Introduction

This script written as a solution for an assignment of a subject in my colledge (Information System). It take input in form of an assembly-like source file following a dead-simple instruction set - to perform arithmetic calculations; then output a C file following ANSI C standard, which can then be compiled by any C compiler and executed.

# Requirement

* Python 3
* Optional (Linux):
  * GCC
  * GNU indent

# Quickstart

## Windows

Run in cmd:

```bat
.\compile.exe <source filename>
```

For example:

```bat
.\compile.exe example.txt
```

## Linux

```bash
python compile.py <source filename>
```

Target C code will be written in a file with the same basename as source file.

For example:

```bash
python compile.py example.txt
```

Will output `example.c`

# Usage

```
usage: compile.py [-h] [-o OUTPUT] [-c] [-r] [-b] filename

Stupid compiler. Refer to README.md for further details.

positional arguments:
  filename              Specify source filename

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output filename of target C file (default to source
                        file's basename with .c suffix)
  -c, --compile-c       Directly compile target C file to executable (Linux
                        only, require GCC, generate a.out)
  -r, --run             Directly compile to executable then execute it (Linux
                        only)
  -b, --beautify        Format target C code following the prominent style
                        guide from "The C Programming Language" (Linux only,
                        require GNU indent)

```

# Instruction spec

```
source := instruction + newline + source | null
ínstruction := input_inst | output_inst | add_inst
               | subtract_inst | multiply_inst | divide_inst

input_inst := 'INPUT' + space + var_list
output_inst := 'OUTPUT' + space + var_list
var_list := var_name + space + var_list | null

add_inst := 'ADD' + space + term1 + space + term2 + space + result
subtract_inst := 'SUB' + space + subtrahend + space + minus + space + result
multiply_inst := 'MUL' + space + factor1 + space + factor2 + space + result
divide_inst := 'DIV' + space + dividend + space + divisor + space + result

newline := '\n'
space := ' '
```
